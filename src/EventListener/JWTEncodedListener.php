<?php
namespace App\EventListener;
// src/App/EventListener/JWTEncodedListener.php

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTEncodedEvent;


class JWTEncodedListener {
    
    /**
     * @param JWTEncodedEvent $event
     */
    public function onJwtEncoded(JWTEncodedEvent $event)
    {
        $token = $event->getJWTString();
    }
}